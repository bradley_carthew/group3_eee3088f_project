Make sure fiducials are a small piece of exposed copper and nothing else.
Include LED colour on each of the LEDs (green, red and white LEDs).
Use reels where possible and if not possible then allow for a length of loss on the reel.
If possible, place a crosshair on the overlay in the centre of each IC.
Ensure the packages fit correctly.
Implement tombstoning.
Make sure PCB structural integrity is sufficient.
Stencils should be produced by the manufacturer (do not use the original paste files).
