The following is the Bill of Materials for the micro piHAT. In this excel spreadsheet all the required components and the total price is included. The prices for the components were obtained from the following store 
    RS Components(SA) https://za.rs-online.com/web/ 
    DIY Electronics https://www.diyelectronics.co.za/store/resistors/1567-resistor-1k-ohm-14w-5.html
    Digi-Key electronics https://www.digikey.co.za/en
    Ubuy https://www.ubuy.za.com/
