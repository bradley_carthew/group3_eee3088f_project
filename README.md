# Group3_EEE3088F_Project
**microHAT Discription**

The microHAT will be able to drive a small motor and provide feedback on the position and speed of the motor. The microHAT will consist of a voltage regulator circuit, a power supply, a motor that provides position and speed feedback, LEDs which indicate the different statuses of the system (on/off, if battery needs to be replaced and motor direction) and a ZVD circuit to protect the Raspberry Pi Zero. Some scenarios in which the microHAT would be useful are: remote controlled cars, a moving solar panel, shop counter conveyor belt, motorised curtains, projector screens, small garage doors, etc

**I. Whats Here**
        
The contents of this repository is the KiCAD and LTSpice files to run simmulations of the microHAT with motor feedback. The LTSpice file for each individual sub system and KiCAD schematics are available in the relevant under the names KICAD files and LTSpice files.        

**II. How To Install The microHAT**
        
Download the files from GITLab or using a shell (terminal/GIT bash) clone the repository to your device. To access the simmulation files you will need to download LTSpice, and to access the schematic and pcb files you will need to download KiCAD. 

**III. How To Use The microHAT**

The microHAT can be used to be attached to the a raspberry Pi zero. The microHAT can be used fo ran system that requires a motor and need to be given motor feedback. 

**IV. An Example of Using The microHAT**

remote controlled cars, a moving solar panel, shop counter conveyor belt, motorised curtains, projector screens, small garage doors, etc

**V. Usage Rights**

Copyrights 2021 Bradley Carthew, Thomas Stern, Nathanael Thomas

Please refer to the file named LICENSE for complete license text.

Group Members

    Bradley Carthew 
    Thomas Stern
    Nathanael Thomas
