EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Switching:LT1073CN-5 U1
U 1 1 60B3D8F9
P 4700 3000
F 0 "U1" H 4257 3046 50  0000 R CNN
F 1 "LT1073CN-5" H 4257 2955 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 4750 2650 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/1073fa.pdf" H 4500 3600 50  0001 C CNN
	1    4700 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 60B3ED5B
P 4500 2500
F 0 "R1" H 4559 2546 50  0000 L CNN
F 1 "220" H 4559 2455 50  0000 L CNN
F 2 "" H 4500 2500 50  0001 C CNN
F 3 "~" H 4500 2500 50  0001 C CNN
	1    4500 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2400 4700 2400
Wire Wire Line
	4700 2400 4700 2600
Wire Wire Line
	5100 2800 5100 2400
Wire Wire Line
	5100 2400 4700 2400
Connection ~ 4700 2400
Connection ~ 4500 2400
$Comp
L Diode:1N5818 D1
U 1 1 60B402B7
P 4700 3550
F 0 "D1" V 4654 3630 50  0000 L CNN
F 1 "1N5818" V 4745 3630 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 4700 3375 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 4700 3550 50  0001 C CNN
	1    4700 3550
	0    1    1    0   
$EndComp
$Comp
L Device:L_Small L1
U 1 1 60B4AADB
P 4950 3400
F 0 "L1" H 4998 3446 50  0000 L CNN
F 1 "100uF" H 4998 3355 50  0000 L CNN
F 2 "" H 4950 3400 50  0001 C CNN
F 3 "~" H 4950 3400 50  0001 C CNN
	1    4950 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4500 3400 4500 3700
Wire Wire Line
	4500 3700 4700 3700
Connection ~ 4700 3400
$Comp
L Device:CP_Small C1
U 1 1 60B42830
P 5200 3550
F 0 "C1" H 5288 3596 50  0000 L CNN
F 1 "100uF" H 5288 3505 50  0000 L CNN
F 2 "" H 5200 3550 50  0001 C CNN
F 3 "~" H 5200 3550 50  0001 C CNN
	1    5200 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3000 5200 3000
Wire Wire Line
	5200 3700 4700 3700
Connection ~ 4700 3700
$Comp
L power:GND #PWR01
U 1 1 60B527DF
P 5200 3700
F 0 "#PWR01" H 5200 3450 50  0001 C CNN
F 1 "GND" H 5205 3527 50  0000 C CNN
F 2 "" H 5200 3700 50  0001 C CNN
F 3 "" H 5200 3700 50  0001 C CNN
	1    5200 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3000 5300 3000
Connection ~ 5200 3000
Wire Wire Line
	4300 2400 4500 2400
Text Label 4300 2400 0    50   ~ 0
+9V
Text Label 5300 3000 0    50   ~ 0
+5V
Wire Wire Line
	5200 3000 5200 3400
Wire Wire Line
	4700 3400 4850 3400
Wire Wire Line
	5050 3400 5200 3400
Connection ~ 5200 3400
Wire Wire Line
	5200 3400 5200 3450
Wire Wire Line
	5200 3700 5200 3650
Connection ~ 5200 3700
Text Notes 7150 6900 0    157  ~ 0
EEE3088F - Hand_in_3
Text Notes 7450 7500 0    50   ~ 0
Power Supply\n
Text Notes 8150 7650 0    50   ~ 0
31-05-2021
Text Notes 8200 7300 0    50   ~ 0
CRTBRA002, THMNAT011, STRTHO008
$EndSCHEMATC
